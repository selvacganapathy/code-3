<?php 

namespace App\Repositories;
 
class UserRepo extends Repository {

  public function transform($items)
    {
        return [
          'username'        => $items['username'],
          'forename'        => $items['forename'],
          'ddi_telephone'   => $items['ddi_telephone'],
          'surname'         => $items['surname'],
          'userType'        => $items['userType'],
          'department'      => $items['department'],
          'jobRoleID'       => $items['jobRoleID']
        ];
    }
}
