<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests; 
use Response;
use App\Helpers;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Logger
{
	public function __construct($type = 'Logger::INFO',$action = null)
	{
		$this->type = $type;

		$this->action 	= $action;
	}
	public static function GeneralLog($type,$message,$filename)
	{
		if($type =='info')
		{
			$type = Logger::INFO;
		}
		else
		{
			$type = Logger::ERROR;
		}

		$view_log = new Logger('Logs');
        
        $view_log->pushHandler(new StreamHandler(storage_path('logs/'.$filename.'.log'), Logger::INFO));

        $view_log->addInfo($message);
	}

	public static function Logger($type,$message,$filename)
	{
		self::GeneralLog($type,$message,$filename);
	}
// 	Log::emergency($error);
// Log::alert($error);
// Log::critical($error);
// Log::error($error);
// Log::warning($error);
// Log::notice($error);
// Log::info($error);
// Log::debug($error);
}
?>