<?php

namespace App\Models\userdb;

use Illuminate\Database\Eloquent\Model;

class Addresses extends Model
{
    /**
	 * $connection - mysql PDO connection driver to databases
	 * $primaryKey - primary key of the table
	 * 
	 */
    protected $connection = "mysql";
    
    protected $primaryKey = "id";

    protected $table = "addresses";

    protected $fillable = [
    					'address_line_1',
    					'address_line_2',
    					'address_line_2',
    					'postcode',
    					'status',
    					'created',
    					'modified',
    ];
}
