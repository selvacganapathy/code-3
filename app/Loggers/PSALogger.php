<?php 
/**
 * logger type : error, activity and access
 * Monolog recognizes the following severity levels - from least severe to most severe: 
 * debug,
 * info, 
 * notice, 
 * warning, 
 * error, 
 * critical, 
 * alert, 
 * emergency.
 */
use App\Helpers;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Routing\Route;

class PSALogger
{
	public function __construct($type = 'Logger::INFO',$action = null)
	{
		$this->type = $type;

		$this->action 	= $action;
	}

	public static function logger($type,$message,$filename)
	{
		if($type =='info')
		{
			$type = Logger::INFO;
		}
		else
		{
			$type = Logger::ERROR;
		}

		$view_log = new Logger('Logs');
        
        $view_log->pushHandler(new StreamHandler(storage_path('logs/'.$filename.'-'.date("d-m-Y").'.log'), Logger::INFO));

        $view_log->addInfo($message);
	}

	public static function Error($message)
	{
		self::logger('error',$message,'Error');
	}

	public static function Activity($message)
	{
		self::logger('info',$message,'Activity');
	}

	public static function Access($message)
	{
		self::logger('info',$message,'Access');
	}
}
 ?>
