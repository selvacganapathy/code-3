<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests; 
use Response;
class StatusController extends Controller 
{
  
  protected $statusCode = 200;

  public function getStatusCode()
  {

    return $this->statusCode;

  }
  public function setStatusCode($statusCode)
  {

    $this->statusCode = $statusCode;

    return $this;
  }
  
  public function response($data,$headers = [])
  {

    return Response::json($data,$this->getStatusCode());

  }
  public function respond($data)
  {
    return Response::json($data, $this->getStatusCode());
  }

  public function responseNotFound($message = 'Not Found',$statusCode)
  {
    return $this->setStatusCode($statusCode)->responseWithError($message);
  }
  
  public function responseWithError($message)
  {

    return $this->respond(
      [
        'error' => $message,
        'statusCode'  =>  $this->getStatusCode()
    ]);
  }

}
?>