<?php 

namespace App\Repositories;
 
class DealershipRepo extends Repository {

  public function transform($items)
    {
        return [
          /**
           * DEALERSHIP
           */
          'dealerName'        => $items->name,
          'dealerCode'        => $items->code,
          'rrdiCode'          => $items->rrdi,
          'telephone'         => $items->telephone,
          'fax'               => $items->fax, 
          /**
           * DEALER GROUP
           **/         
          'groupName'         => $items->dealerGroup->name,
          'groupCode'         => $items->dealerGroup->code,
          /**
           * ADDRESS
           */
          'address1'          => $items->address->address_line_1,
          'address2'          => $items->address->address_line_2,
          'address3'          => $items->address->town_city,
          'address4'          => $items->address->county,
          'postcode'          => $items->address->postcode,
        ];
    }
}
