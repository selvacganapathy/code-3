<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
	/**
	 * $connection - mysql PDO connection driver to databases
	 * $primaryKey - primary key of the table
	 * 
	 */
	protected $connection = "apiaccess";
    
    protected $table = "agency";

    protected $primaryKey = 'id';
    
    protected $fillable = [
    		'agency_name',
    		'is_active'
    ];
}