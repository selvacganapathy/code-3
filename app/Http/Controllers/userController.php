<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\userdb\Users as DBUsers;
use App\Models\userdb\Dealership;
use App\Models\Access\agency;
use App\Repositories\UserRepo;

class userController extends StatusController
{
	protected $UserRepo;

	function __construct(UserRepo $UserRepo)
    {
        // $this->middleware('auth.basic');

        $this->UserRepo = $UserRepo;

    }

    public function index() 
    {
        return $this->responseNotFound('Unauthorised Access',404);
    }

}
